const express = require('express');
const app = express();
const port = process.env.PORT || 3030;

app.use(express.static('src/public'));

app.listen(port, function () {
    console.log(`Example app listening on port ${port}`);
});